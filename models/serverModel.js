var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ServerMC = new Schema({
    username: String,
    date: { type: Date, default: Date.now },
    server_name: String,
    server_ip: String,
    server_port: Number,
    server_desc: String,
    server_img: String,
    server_type: String,
    votes: Number,
    votifier_key: String,
    votifier_port: Number
});

module.exports = mongoose.model('ServerMC', ServerMC);
