var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var ServerMC = require('../models/serverModel');
var router = express.Router();
var https = require('https');
var PUBLIC = '6LdlFx8TAAAAAEYQlY2yPSsFLSxJAnGO5lkk_M_u';
var SECRET = '6LdlFx8TAAAAAICPteGVeIJ0aT5kNMus1PbvV74J';

router.get('/', function(req, res) {
  ServerMC.find({}, function(err, servers) {
    res.render('index', {
        user: req.user,
        page: "home",
        serverlist: servers
    });
  });
});

router.get('/servers', function(req, res) {
    ServerMC.find({}, function(err, servers) {
      res.render('servers', {
          user: req.user,
          page: "servers",
          serverlist: servers
      });
    });
});

router.get('/server/', function(req, res) {
  ServerMC.findOne({'_id': req.query.id}, function(err, serverInfo) {
    res.render('server', {
        user: req.user,
        page: "server",
        id: req.query.id,
        serverInfo: serverInfo
    });
  });
});

router.get('/addServer', function(req, res) {
    if (req.user) {
        res.render('addServer', {
            user: req.user,
            page: "addServer",
            error: req.query.error,
        });
    } else {
        res.redirect('/');
    }
});

router.post('/addServer', function(req, res) {
    if (!req.user) {
        res.redirect('/');
        return;
    }
    // TODO: ADD SERVER IN TO MONGO DB COLLECTION (!) SCHEMA DONE

    ServerMC.count({
        username: req.user.username
    }, function(err, count) {
        if (count > 0) {
            res.redirect('/addServer?error=1');
            return;
        } else {
            ServerMC.create({
                username: req.user.username,
                server_name: req.body.serverName,
                server_ip: req.body.serverIp,
                server_port: req.body.serverPort,
                server_desc: req.body.serverDesc,
                server_img: req.body.serverImg,
                server_type: req.body.serverType,
                votes: 0,
                votifier_key: req.body.votifierKey,
                votifier_port: req.body.votifierPort
            }, function(err, server) {
                if (err) return res.end('err');
                res.render('index', {
                    user: req.user,
                    page: 'home',
                    error: '3'
                });
            })
        }
    });
});


router.get('/register', function(req, res) {
    if (!req.user) {
        res.render('register', {
            user: req.user,
            page: "register",
        });
    } else {
        res.redirect('/');
    }
});

router.post('/register', function(req, res, next) {
    if (!req.user) {
        verifyRecaptcha(req.body["g-recaptcha-response"], function(success) {
            if (success) {
                Account.register(new Account({
                    username: req.body.username,
                    email: req.body.email,
                }), req.body.password, function(err, account) {
                    if (err) {
                        return res.render('register', {
                            error: err.message
                        });
                    }
                    passport.authenticate('local')(req, res, function() {
                        req.session.save(function(err) {
                            if (err) {
                                return next(err);
                            }
                            res.redirect('/');
                        });
                    });
                });
            } else {
                res.end("Captcha failed, sorry.");
                // TODO: take them back to the previous page
                // and for the love of everyone, restore their inputs
                res.redirect("/register");
            }
        });
    } else {
        return;
    }
});

function verifyRecaptcha(key, callback) {
    https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + SECRET + "&response=" + key, function(res) {
        var data = "";
        res.on('data', function(chunk) {
            data += chunk.toString();
        });
        res.on('end', function() {
            try {
                var parsedData = JSON.parse(data);
                callback(parsedData.success);
            } catch (e) {
                callback(false);
            }
        });
    });
}



router.get('/login', function(req, res) {
    if (!req.user) {
        res.render('login', {
            user: req.user,
            page: 'login'
        });
    } else {
        res.redirect('/');
    }
});

router.post('/login', passport.authenticate('local'), function(req, res) {
    res.redirect('/');
});

router.get('/logout', function(req, res) {
    if (req.user) {
        req.logout();
        res.redirect('/');
    } else {
        res.redirect('/');
    }
});


module.exports = router;
